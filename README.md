# Bedrijven Contactdagen portal

This portal is to be used for possible future interns to orient themselfs on the availble intern assignements.

- Clone this repo
- Run `npm install`
- Run `gatbsy develop`

---

- Run `npm start:lambda` to start a local instance of the "serverless" functions

The serverless functions are found in the `functions` folder, located in the project root.
