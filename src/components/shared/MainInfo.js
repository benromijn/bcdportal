import styled from 'styled-components'

const MainInfo = styled.div`
  padding-left: 0;
  font-weight: 300;
  font-size: 16px;
  @media (min-width: ${props => props.theme.bpmdup}) {
    max-width: 70vw;
    padding-left: 50px;
  }
`
export default MainInfo
