import styled from 'styled-components'
import { Link } from 'gatsby'

const Button = styled(Link)`
  background: ${props => (props.primary ? props.theme.colorPrimary : 'white')};
  border: none;
  border-radius: 50px;
  padding: 0 40px;
  display: inline-block;
  color: white;
  text-transform: uppercase;
  height: 40px;
  line-height: 40px;
  font-family: MontserratSemiBold;
  font-size: 15px;
  margin-top: ${props => (props.margintop ? props.margintop + 'px' : '0')};
  margin-bottom: ${props =>
    props.marginbottom ? props.marginbottom + 'px' : '0'};
  cursor: pointer;
  text-decoration: none;
`

export default Button
