import styled from 'styled-components'
import AnchorLink from 'react-anchor-link-smooth-scroll'

const ScrollButton = styled(AnchorLink)`
  background: ${props => (props.primary ? props.theme.colorPrimary : 'white')};
  border: none;
  border-radius: 50px;
  padding: 0 40px;
  display: inline-block;
  color: white;
  text-transform: uppercase;
  height: 40px;
  line-height: 40px;
  font-family: MontserratSemiBold;
  font-size: 15px;
  margin-top: ${props => (props.margintop ? props.margintop + 'px' : '0')};
  margin-left: ${props => (props.marginleft ? props.marginleft + 'px' : '0')};
  align-self: flex-start;
  cursor: pointer;
  text-decoration: none;
`

export default ScrollButton
