import styled from 'styled-components'

const Container = styled.div`
  margin: 0 auto;
  padding: 0 40px;
  height: 100%;
  @media (min-width: ${props => props.theme.bpsmup}) {
    width: 750px;
    padding: 0 40px;
  }
  @media (min-width: ${props => props.theme.bpmdup}) {
    padding: 0 20px;
    width: 970px;
  }
  @media (min-width: ${props => props.theme.bplgup}) {
    width: 1170px;
  }
`

export default Container
