import styled from 'styled-components'
import React from 'react'
import Container from './Container'

const FlexWrapper = styled(Container)`
  display: flex;
  flex-flow: column nowrap;
  justify-content: ${props =>
    props.flexposition ? props.flexposition : 'flex-start'};

  @media (min-width: ${props => props.theme.bpsmup}) {
    justify-content: center;
  }
`

const Wrapper = ({ children, flexposition }) => (
  <FlexWrapper flexposition={flexposition}>{children}</FlexWrapper>
)

export default Wrapper
