import styled from 'styled-components'

const PaddedSubtitle = styled.h2`
  color: ${props => props.theme.colorPrimary};
  margin-bottom: 40px;
  padding-left: 0;
  @media (min-width: ${props => props.theme.bpmdup}) {
    padding-left: 50px;
  }
`
export default PaddedSubtitle
