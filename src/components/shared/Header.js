import React from 'react'
import styled from 'styled-components'
import HeaderLogo from './HeaderLogo'
import headerImage from '../../images/bear_green.svg'
import { Link } from 'gatsby'

const HeaderBackground = styled.div`
  background: white;
  height: 80px;
  padding-left: 40px;
  display: flex;
  align-items: center;
  position: fixed;
  width: 100%;
  top: -80px;
  transition: top 0.4s ease, box-shadow 0.4s ease;
  z-index: 100;
  &.active {
    box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2);
    top: 0;
  }
  img {
    height: 32px;
  }
`

export default class Header extends React.Component {
  render() {
    if (typeof window !== `undefined`) {
      let last_known_scroll_position = 0
      window.addEventListener('scroll', function(e) {
        last_known_scroll_position = window.scrollY
        if (last_known_scroll_position >= '300') {
          document.getElementById('header').classList.add('active')
        } else {
          document.getElementById('header').classList.remove('active')
        }
      })
    }
    return (
      <HeaderBackground id="header">
        <Link to="/" aria-label="Terug naar de hoofd pagina">
          <HeaderLogo src={headerImage} alt="Arcady_Logo" />
        </Link>
      </HeaderBackground>
    )
  }
}
