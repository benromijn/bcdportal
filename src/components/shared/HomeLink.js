import styled from 'styled-components'
import { Link } from 'gatsby'

const HomeLink = styled(Link)`
  display: block;
  height: 32px;
  left: 40px;
  position: absolute;
  top: 40px;
  width: auto;
  z-index: 200;
`

export default HomeLink
