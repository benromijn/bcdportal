import styled from 'styled-components'

const HeaderLogo = styled.img`
  order: 0;
  display: block;
  height: 100%;
`
export default HeaderLogo
