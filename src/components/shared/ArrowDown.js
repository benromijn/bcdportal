import React from 'react'
import styled from 'styled-components'
import arrowDown from '../../images/arrow-down.svg'
import AnchorLink from 'react-anchor-link-smooth-scroll'
import { bounce } from '../styles/keyframes'

const BouncingArrow = styled(AnchorLink)`
  position: absolute;
  bottom: 0;
  width: 50px;
  margin-left: -25px;
  left: 50%;
  display: block;
  animation: ${bounce} infinite 0.7s;
  animation-duration: 1s;
  animation-iteration-count: infinite;
  animation-direction: alternate;
  z-index: 12;
  img {
    width: 100%;
  }
`

const ArrowDown = props => (
  <BouncingArrow offset="80" href={props.target}>
    <img alt="Next page" src={arrowDown} />
  </BouncingArrow>
)

export default ArrowDown
