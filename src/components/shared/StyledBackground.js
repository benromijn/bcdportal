import styled from 'styled-components'
import bgtl from '../../images/bg-topleft.png'
import bgtr from '../../images/bg-topright.png'

const StyledBackground = styled.div`
  padding: 80px 0;
  background-color: white;
  background-image: url(${bgtl}), url(${bgtr});
  background-position: 0 -40px, 100% -40px;
  background-repeat: no-repeat;
`

export default StyledBackground
