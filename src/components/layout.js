import React from 'react'
import Header from '../components/shared/Header'
import GlobalStyle from '../components/styles/GlobalStyles'
import { ThemeProvider } from 'styled-components'
import { Helmet } from 'react-helmet'
import { theme } from './styles/theme'

const Layout = ({ children, helmet }) => (
  <ThemeProvider theme={theme}>
    <>
      <GlobalStyle />
      {/* Misschien hier nog een helmet specifiek voor de layout */}
      <Helmet>
        <meta charSet="utf-8" />
        <title>{`Arcady | ${helmet}`}</title>
      </Helmet>
      <Header />
      {children}
    </>
  </ThemeProvider>
)

export default Layout
