import React from 'react'
import styled from 'styled-components'
import StyledBackground from '../shared/StyledBackground'
import Container from '../shared/Container'
import PaddedSubtitle from '../shared/PaddedSubtitle'
import MainInfo from '../shared/MainInfo'

const SignUpWrapper = styled.div``

const SubmitButton = styled.button`
  background: ${props => (props.primary ? props.theme.colorPrimary : 'white')};
  border: none;
  border-radius: 50px;
  padding: 0 40px;
  display: inline-block;
  color: white;
  text-transform: uppercase;
  height: 40px;
  line-height: 40px;
  font-family: MontserratSemiBold;
  font-size: 15px;
  margin-top: ${props => (props.margintop ? props.margintop + 'px' : '0')};
  margin-bottom: ${props =>
    props.marginbottom ? props.marginbottom + 'px' : '0'};
  cursor: pointer;
  text-decoration: none;
`

const Flexer = styled.div`
  display: flex;
  flex-flow: row wrap;
  margin-top: 40px;
  justify-content: space-between;
`

const FormPart = styled.div`
  flex: 0 0 100%;
  margin-bottom: 40px;
  @media (min-width: ${props => props.theme.bpmdup}) {
    flex: 0 0 70%;
  }
`

const ContactInfo = styled.div`
  flex: 0 0 100%;
  @media (min-width: ${props => props.theme.bpmdup}) {
    flex: 0 0 25%;
  }
  p {
    font-size: 16px;
    line-height: 28.8px;
  }
`

export default class AssignmentSignUp extends React.Component {
  state = {
    firstName: '',
    lastName: '',
    email: '',
    consent: '',
  }

  handleInputChange = event => {
    const target = event.target
    const value = target.value
    const name = target.name

    this.setState({
      [name]: value,
    })
  }

  render() {
    return (
      <StyledBackground id="signUp">
        <SignUpWrapper>
          <Container>
            <h1>Ben jij de match?</h1>
            <PaddedSubtitle>
              Afstudeerder {this.props.data.frontmatter.title}
            </PaddedSubtitle>
            <MainInfo>
              <p>
                De laatste stap. Laat je naam en email achter. We nemen sowieso
                contact met je op.
              </p>
              <Flexer>
                <FormPart>
                  <form
                    name={this.props.data.frontmatter.title}
                    method="post"
                    data-netlify="true"
                    action="/success/"
                    data-netlify-honeypot="bot-field"
                  >
                    <input type="hidden" name="bot-field" />

                    <div className="formgroup">
                      <label htmlFor="firstName">Voornaam</label>
                      <input
                        type="text"
                        name="firstName"
                        value={this.state.firstName}
                        onChange={this.handleInputChange}
                      />
                    </div>
                    <div className="formgroup">
                      <label htmlFor="lastName">Achternaam</label>
                      <input
                        type="text"
                        name="lastName"
                        value={this.state.lastName}
                        onChange={this.handleInputChange}
                      />
                    </div>
                    <div className="formgroup">
                      <label htmlFor="email">Email</label>
                      <input
                        type="email"
                        name="email"
                        value={this.state.email}
                        onChange={this.handleInputChange}
                      />
                    </div>
                    <div className="formgroup">
                      <input
                        type="checkbox"
                        name="consent"
                        value={this.state.consent}
                        onChange={this.handleInputChange}
                        className="consent"
                        required
                      />
                      <label htmlFor="consent" className="consent">
                        Ik reken er op dat mijn gegevens veilig en alleen intern
                        worden gebruikt, volgens onze&nbsp;
                        <a
                          href="https://arcady.nl/privacy"
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          voorwaarden.
                        </a>
                      </label>
                    </div>
                    <SubmitButton
                      primary="true"
                      type="submit"
                      aria-label="Schrijf je in"
                    >
                      Submit
                    </SubmitButton>
                  </form>
                </FormPart>
                <ContactInfo>
                  <h5>Arcady</h5>
                  <p>
                    Emmastraat 4<br />
                    8011 AG Zwolle
                  </p>

                  <p>
                    038 760 1060
                    <br />
                    <a href="mailto:hello@arcady.nl">hello@arcady.nl</a>
                  </p>
                </ContactInfo>
              </Flexer>
            </MainInfo>
          </Container>
        </SignUpWrapper>
      </StyledBackground>
    )
  }
}
