import React from 'react'
import styled from 'styled-components'
import BackgroundImage from 'gatsby-background-image'

const ImageWrapper = styled.div`
  height: 60vh;
  position: relative;
`
const StyledBackgroundImage = styled(BackgroundImage)`
  &[style] {
    opacity: 1 !important;
  }
`

const ImagesContainer = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-rows: 1fr 1fr;
  box-shadow: inset 0 0 0 2000px rgba(51, 49, 50, 0.8);
  height: 100%;
  opacity: 1;
`

const OverImageText = styled.h1`
  color: white;
  z-index: 10;
  text-align: center;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`

const AssignmentImages = ({ data }) => {
  return (
    <>
      <ImageWrapper>
        <ImagesContainer>
          {data.edges.map(({ node }) => (
            <StyledBackgroundImage
              key={node.childImageSharp.fluid.tracedSVG}
              fluid={node.childImageSharp.fluid}
            />
          ))}
        </ImagesContainer>
        <OverImageText>Work hard. Play hard.</OverImageText>
      </ImageWrapper>
    </>
  )
}

export default AssignmentImages
