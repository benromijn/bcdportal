import styled from 'styled-components'
import React from 'react'
import icon from '../../images/bear_green.svg'

const Flexer = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  padding-left: 0;
  @media (min-width: ${props => props.theme.bpmdup}) {
    padding-left: 50px;
  }
`

const Column = styled.div`
  flex: 0 0 100%;
  @media (min-width: ${props => props.theme.bpmdup}) {
    flex: 0 0 45%;
  }

  h4 {
    font-family: MontserratSemiBold;
    letter-spacing: 0;
    margin-bottom: 20px;
  }
  ul {
    padding: 0;
    margin: 0;
  }

  li {
    list-style: none;
    font-weight: 300;
    margin-right: 35px;
    margin-bottom: 10px;
    padding-left: 35px;
    line-height: 1.5;
    background-repeat: no-repeat;
    background-image: url(${icon});
    background-size: 24px;
    background-position: 0% 5px;
  }
`

const Benefits = props => (
  <Flexer>
    <Column>
      <h4>Waarom je het echt wilt.</h4>
      <ul>
        <li>De beste {props.title} worden</li>
        <li>Ruimte voor flexibiliteit en creativiteit.</li>
        <li>Afstuderen tussen experts</li>
        <li>Gewaardeerd en gerespecteerd worden door collega's.</li>
        <li>Vrijdagmiddagbier en PS4 of tafelvoetbal challenges</li>
      </ul>
    </Column>
    <Column>
      <h4>Wat er ook bij hoort.</h4>
      <ul>
        <li>Fijne werksfeer in een mooi pand in de Zwolse binnenstad</li>
        <li>Loopafstand van het station</li>
        <li>Stagevergoeding</li>
        <li>Professionele begeleiding</li>
        <li>En nog veel meer...</li>
      </ul>
    </Column>
  </Flexer>
)

export default Benefits
