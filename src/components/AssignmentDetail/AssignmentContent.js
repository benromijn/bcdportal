import React from 'react'
import styled from 'styled-components'
import Container from '../shared/Container'
import Benefits from '../AssignmentDetail/Benefits'
import StyledBackground from '../shared/StyledBackground'
import ScrollButton from '../shared/ScrollButton'
import PaddedSubtitle from '../shared/PaddedSubtitle'
import MainInfo from '../shared/MainInfo'

const Padder = styled.div`
  padding: 80px 0;
`

const AssignmentContent = props => (
  <>
    <Container id="nextPart">
      <Padder>
        <h1>TL;DR.</h1>
        <PaddedSubtitle>Eerst even de feiten</PaddedSubtitle>
        <Benefits title={props.data.frontmatter.title} />
      </Padder>
    </Container>
    <StyledBackground>
      <Container>
        <h1>Dit ga jij doen.</h1>
        <PaddedSubtitle>En waarom dat er toe doet.</PaddedSubtitle>
        <MainInfo dangerouslySetInnerHTML={{ __html: props.data.html }} />
        <ScrollButton
          primary="true"
          margintop="40"
          href="#signUp"
          marginleft="50"
          offset="80"
          aria-label="Go down"
        >
          Sign me up!
        </ScrollButton>
      </Container>
    </StyledBackground>
  </>
)

export default AssignmentContent
