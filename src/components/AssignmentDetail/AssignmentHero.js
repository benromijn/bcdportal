import React from 'react'
import styled from 'styled-components'
import ArrowDown from '../shared/ArrowDown'
import BackgroundImage from 'gatsby-background-image'

const Hero = styled(BackgroundImage)`
  height: 70vh;
  position: relative;
  /* Fugly */
  &[style] {
    opacity: 1 !important;
  }
  box-shadow: inset 0 0 0 2000px rgba(51, 49, 50, 0.8);
  opacity: 1;
`

const AssignmentHero = ({ children, background }) => (
  <Hero fluid={background.childImageSharp.fluid}>
    {children}
    <ArrowDown target="#nextPart" />
  </Hero>
)

export default AssignmentHero
