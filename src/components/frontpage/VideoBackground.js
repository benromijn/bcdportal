import styled from 'styled-components'
import React from 'react'
import video from '../../images/arcady_video.mp4'
import fallback from '../../images/fallback-image.png'
import { fadeInVideo } from '../styles/keyframes'

const VideoContainer = styled.div`
  position: fixed;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  background-color: #222;
  z-index: -100;
  box-shadow: inset 0 0 0 2000px rgba(51, 49, 50, 0.8);
`
const BackgroundVideo = styled.video`
  position: fixed;
  right: 0;
  bottom: 0;
  min-width: 100%;
  min-height: 100%;
  width: auto;
  height: auto;
  opacity: 0;
  animation: ${fadeInVideo} ease-in 1;
  animation-fill-mode: forwards;
  animation-duration: 1s;
`

const VideoBackground = () => (
  <VideoContainer>
    <BackgroundVideo className="videoTag" autoPlay loop muted playsInline>
      <source src={video} type="video/mp4" />
      <img src={fallback} alt="Video laadt niet in" />
    </BackgroundVideo>
  </VideoContainer>
)

export default VideoBackground
