import React from 'react'
import styled, { keyframes } from 'styled-components'
import Container from '../shared/Container'
import IntroPart from './IntroPart'
import ClicketyClack from 'react-clicketyclack'

const blink = keyframes`
  from {
      opacity: 0
    }
  to {
    opacity: 1
  }
`

const Background = styled.div`
  background: white;
  width: 100%;
  position: relative;

  h1 {
    margin-bottom: 40px;
  }
  .clicketyClack-cursor {
    animation: ${blink} 0.8s infinite linear;
  }
`

const lines = ['backenders', 'frontenders', 'fullstackers']

export default class Content extends React.Component {
  render() {
    return (
      <Background id="secondPage">
        <Container>
          <IntroPart>
            <h1>
              <span className="primary">Arcady houdt van </span>
              <ClicketyClack
                lines={lines}
                repeat="true"
                erase="true"
                pause={1500}
              />
              <span className="clicketyClack-cursor">|</span>
            </h1>
            <p>
              Arcady houdt van veel. Vooral ook van uitdagingen. Want daarvan
              maken wij oplossingen. Slimme software met impact. Op merken en
              mensen, op vandaag en morgen.
            </p>
            <p>
              Noem ons meerwaardemakers, techdenkers of toekomstbouwers. We zijn
              bovenal businesspartner, met goede ideeën én resultaten. Daar
              houden we het meest van. En daarom kies je voor Arcady.
            </p>
          </IntroPart>
        </Container>
      </Background>
    )
  }
}
