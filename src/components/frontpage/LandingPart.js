import React from 'react'
import styled from 'styled-components'
import VideoBackground from './VideoBackground'
import Wrapper from '../shared/Wrapper'
import ScrollButton from '../shared/ScrollButton'
import ArrowDown from '../shared/ArrowDown'
import HeaderLogo from '../shared/HeaderLogo'
import logo from '../../images/arcady_logo_white_64.png'
import HomeLink from '../shared/HomeLink'

const MainFrame = styled.div`
  box-shadow: inset 0 0 0 2000px rgba(51, 49, 50, 0.3);
  height: 100vh;
`

const VideoHeader = styled.h1`
  color: white;
  padding-top: 100px;
  @media (min-width: ${props => props.theme.bpsmup}) {
    padding-top: 0;
  }
`

const VideoSubHeader = styled.h2`
  font-size: 24px;
  line-height: 28.8px;
  color: ${props => props.theme.colorPrimary};
  @media (min-width: ${props => props.theme.bpmdup}) {
    font-size: 36px;
    line-height: 43px;
  }
`

const LandingPart = () => (
  <MainFrame>
    <HomeLink to="/" alt="Home">
      <HeaderLogo src={logo} alt="arcady logo" />
    </HomeLink>
    <Wrapper>
      <VideoHeader>
        <div>Je wilt vooroplopen.</div>
        <div>Meer dan één stap.</div>
        <div>Wij geven je die voorsprong.</div>
      </VideoHeader>
      <VideoSubHeader>
        <div>Want wij zijn Arcady. We ontwikkelen software.</div>
        <div>En jouw toekomst.</div>
      </VideoSubHeader>
      <ScrollButton
        offset="80"
        href="#secondPage"
        primary="true"
        margintop="40"
        aria-label="Go down"
      >
        Iets voor jou?
      </ScrollButton>
      <ArrowDown target="#secondPage" />
    </Wrapper>
    <VideoBackground />
  </MainFrame>
)
export default LandingPart
