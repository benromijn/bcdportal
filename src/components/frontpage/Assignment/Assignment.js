import React, { Component } from 'react'
import styled from 'styled-components'
import Button from '../../shared/Button'

const AssignmentWrapper = styled.div`
  flex: 0 0 100%;
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  @media (min-width: ${props => props.theme.bpsmup}) {
    flex: 0 0 30%;
  }

  h4 {
    margin-bottom: 20px;
    font-size: 24px;
    line-height: 28.8px;
    text-align: center;
  }
  p {
    font-size: 16px;
    line-height: 28.8px;
    text-align: center;
    font-weight: 300;
  }
`

const Icon = styled.div`
  width: 90px;
  height: 90px;
  border: 3px solid #00a667;
  border-radius: 90px;
  margin: 0 auto 20px;
  img {
    margin: 20px auto 0;
    display: block;
    width: 45px;
    height: auto;
  }
`

export default class Assignment extends Component {
  clicker = async () => {
    const response = await fetch('/.netlify/functions/test')
    // const response = await fetch('http://localhost:9000/test')
    const resObj = await response.json()
    console.log(resObj.employee)
    // resObj.map(person => {
    //   console.log(person.firstName)
    // })
  }

  render() {
    const { data } = this.props
    return (
      <AssignmentWrapper>
        <Icon onClick={() => this.clicker()}>
          <img
            alt={data.frontmatter.title}
            src={data.frontmatter.icon.childImageSharp.fixed.src}
          />
        </Icon>
        <h4>{data.frontmatter.title}</h4>
        <p>{data.frontmatter.intro}</p>
        <Button
          marginbottom={60}
          to={data.fields.slug}
          aria-label="Deze opdracht is voor mij"
          primary="true"
        >
          Deze is voor mij!
        </Button>
      </AssignmentWrapper>
    )
  }
}
