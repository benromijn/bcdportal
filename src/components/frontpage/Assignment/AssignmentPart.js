import React from 'react'
import Assignment from './Assignment'
import Container from '../../shared/Container'
import styled from 'styled-components'
import StyledBackground from '../../shared/StyledBackground'

const Flexer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-flow: row wrap;
`

export default class AssignmentPart extends React.Component {
  render() {
    const { data } = this.props
    const assignments = data.allMarkdownRemark.edges
    return (
      <StyledBackground>
        <Container>
          <Flexer>
            {assignments.map(({ node }) => (
              <Assignment key={node.id} data={node} />
            ))}
          </Flexer>
        </Container>
      </StyledBackground>
    )
  }
}
