// import React from 'react'
import styled from 'styled-components'

const IntroPart = styled.div`
  padding: 80px 0;
  text-align: center;
  h1 {
    text-align: left;
  }
  p {
    font-size: 18px;
    line-height: 32.4px;
    font-weight: 300;
    margin-left: auto;
    margin-right: auto;
    @media (min-width: ${props => props.theme.bpmdup}) {
      max-width: 80%;
      line-height: 37.8px;
      font-size: 21px;
    }
  }
`

export default IntroPart
