const theme = {
  colorPrimary: '#00a667',
  colorSecondary: '#333132',
  bpsmup: '768px',
  bpmdup: '992px',
  bplgup: '1200px',
  // bs: '0 12px 24px 0 rgba(0,0,0,0.09)',
}

export { theme }
