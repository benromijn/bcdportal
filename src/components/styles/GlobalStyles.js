import { createGlobalStyle } from 'styled-components'
import fontFiles from './fonts'

const GlobalStyle = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Roboto:300,400');


  @font-face {
    font-family: MontserratBold;
    font-style: normal;
    font-weight: normal;
    src:  local("montserrat bold"), 
          local("montserrat-bold"), 
          url(${fontFiles.MontserratBoldTTF}) format("truetype"), 
          url(${fontFiles.MontserratBoldWOFF}) format("woff"),
          url(${fontFiles.MontserratBoldWOFF2}) format("woff2"),
          url(${fontFiles.MontserratBoldEOT}) format('embedded-opentype'),
          url(${fontFiles.MontserratBoldSVG}) format("svg");
  }

  @font-face {
    font-family: MontserratSemiBold;
    font-style: normal;
    font-weight: normal;
    src:  local("montserrat semibold"), 
          local("montserrat-semibold"), 
          url(${fontFiles.MontserratSemiBoldTTF}) format("truetype"), 
          url(${fontFiles.MontserratSemiBoldWOFF}) format("woff"),
          url(${fontFiles.MontserratSemiBoldWOFF2}) format("woff2"),
          url(${fontFiles.MontserratSemiBoldEOT}) format('embedded-opentype'),
          url(${fontFiles.MontserratSemiBoldSVG}) format("svg");
  }

  @font-face {
    font-family: MontserratLight;
    font-style: normal;
    font-weight: normal;
    src:  local("montserrat light"), 
          local("montserrat-light"), 
          url(${fontFiles.MontserratLightTTF}) format("truetype"), 
          url(${fontFiles.MontserratLightWOFF}) format("woff"),
          url(${fontFiles.MontserratLightWOFF2}) format("woff2"),
          url(${fontFiles.MontserratLightEOT}) format('embedded-opentype'),
          url(${fontFiles.MontserratLightSVG}) format("svg");
  }

  @font-face {
    font-family: MontserratExtraLight;
    font-style: normal;
    font-weight: normal;
    src:  local("montserrat extralight"), 
          local("montserrat-extralight"), 
          url(${fontFiles.MontserratExtraLightTTF}) format("truetype"), 
          url(${fontFiles.MontserratExtraLightWOFF}) format("woff"),
          url(${fontFiles.MontserratExtraLightWOFF2}) format("woff2"),
          url(${fontFiles.MontserratExtraLightEOT}) format('embedded-opentype'),
          url(${fontFiles.MontserratExtraLightSVG}) format("svg");
  }

  html{
    box-sizing:border-box;
  }

  *,*:before, *:after{
    box-sizing:inherit;
  }

  ::selection {
    background: rgba(0,166,103,.3);
  }

  body{
    padding: 0;
    margin: 0;
    font-family:Roboto;
    min-height: 100%;
    color: ${props => props.theme.colorSecondary}
  }
  a{
    color: ${props => props.theme.colorPrimary}
  }

  h1{
    font-family: MontserratBold, sans-serif;
    color: ${props => props.theme.colorSecondary};
    letter-spacing: -1px;
    margin: 0;
    padding: 0;
    font-weight:normal;
    .primary{
      color: ${props => props.theme.colorPrimary};
    }
    font-size: 32px;
    line-height: 38.4px;
    @media (min-width: ${props => props.theme.bpmdup}) {
      font-size: 50px;
      line-height: 60px;
    }
  }

  h2{
    font-family: MontserratLight, sans-serif;
    font-weight:400;
    font-size: 24px;
    line-height: 28.8px;
    letter-spacing: -1px;
    margin: 0;
    padding: 0;
    @media (min-width: ${props => props.theme.bpmdup}) {
      font-size: 36px;
      line-height:43px;
    }
  }

  h4{
    font-family: MontserratLight, sans-serif;
    font-weight:400;
    letter-spacing: -1px;
    margin: 0;
    padding: 0;
    font-size: 24px;
    line-height:28.8px;
  }

  h5{
    font-family: MontserratSemiBold, sans-serif;
    text-transform: uppercase;
    color: ${props => props.theme.colorPrimary};
    font-size: 15px;
    margin: 0;
    padding: 0;
  }

  p{
    font-size: 18px;
    line-height: 32.4px;
    margin: 0;
    margin-bottom: 20px;

  }

  .formgroup{
    margin-bottom: 40px;
    position: relative;
    label{
    color: ${props => props.theme.colorPrimary};
    font-family: MontserratSemibold;
    display: block;
    margin-bottom: 10px;
    &.consent{
      color: ${props => props.theme.colorSecondary};
      font-size: 15px;
      padding-left: 40px;
      font-family: Roboto;
      font-weight: 300
      }
    }
    input{
    border: 1px solid #e9e9e9;
    font-size: 16px;
    padding: 10px;
    border-radius: 4px;
    width: 100%;
    font-weight: 300;
    &.consent{
      top: 0;
      left: 0;
      position: absolute;
      width:auto;
    }
    }
  }
`
export default GlobalStyle
