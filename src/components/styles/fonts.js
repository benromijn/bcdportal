import MontserratBoldTTF from './fonts/montserrat-bold.ttf'
import MontserratBoldEOT from './fonts/montserrat-bold.eot'
import MontserratBoldWOFF from './fonts/montserrat-bold.woff'
import MontserratBoldWOFF2 from './fonts/montserrat-bold.woff2'
import MontserratBoldSVG from './fonts/montserrat-bold.svg'

import MontserratSemiBoldTTF from './fonts/montserrat-semibold.ttf'
import MontserratSemiBoldEOT from './fonts/montserrat-semibold.eot'
import MontserratSemiBoldWOFF from './fonts/montserrat-semibold.woff'
import MontserratSemiBoldWOFF2 from './fonts/montserrat-semibold.woff2'
import MontserratSemiBoldSVG from './fonts/montserrat-semibold.svg'

import MontserratLightTTF from './fonts/montserrat-light.ttf'
import MontserratLightEOT from './fonts/montserrat-light.eot'
import MontserratLightWOFF from './fonts/montserrat-light.woff'
import MontserratLightWOFF2 from './fonts/montserrat-light.woff2'
import MontserratLightSVG from './fonts/montserrat-light.svg'

import MontserratExtraLightTTF from './fonts/montserrat-extralight.ttf'
import MontserratExtraLightEOT from './fonts/montserrat-extralight.eot'
import MontserratExtraLightWOFF from './fonts/montserrat-extralight.woff'
import MontserratExtraLightWOFF2 from './fonts/montserrat-extralight.woff2'
import MontserratExtraLightSVG from './fonts/montserrat-extralight.svg'

export default {
  MontserratBoldTTF,
  MontserratBoldEOT,
  MontserratBoldWOFF,
  MontserratBoldWOFF2,
  MontserratBoldSVG,

  MontserratSemiBoldTTF,
  MontserratSemiBoldEOT,
  MontserratSemiBoldWOFF,
  MontserratSemiBoldWOFF2,
  MontserratSemiBoldSVG,

  MontserratLightTTF,
  MontserratLightEOT,
  MontserratLightWOFF,
  MontserratLightWOFF2,
  MontserratLightSVG,

  MontserratExtraLightTTF,
  MontserratExtraLightEOT,
  MontserratExtraLightWOFF,
  MontserratExtraLightWOFF2,
  MontserratExtraLightSVG,
}
