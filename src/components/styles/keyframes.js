import { keyframes } from 'styled-components'

const bounce = keyframes`
  from {
      margin-bottom: 0;
    }
  to {
    margin-bottom:20px;
  }
}
`

const fadeInVideo = keyframes`
  from {
      opacity: 0
    }
  to {
    opacity: 1
  }
`

export { bounce, fadeInVideo }
