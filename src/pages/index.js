import React from 'react'
import GlobalStyle from '../components/styles/GlobalStyles'
import Content from '../components/frontpage/Content'
import LandingPart from '../components/frontpage/LandingPart'
import { graphql } from 'gatsby'
import { ThemeProvider } from 'styled-components'
import AssignmentPart from '../components/frontpage/Assignment/AssignmentPart'
import Header from '../components/shared/Header'
import { theme } from '../components/styles/theme'
import { Helmet } from 'react-helmet'

const IndexPage = ({ data }) => (
  <ThemeProvider theme={theme}>
    <React.Fragment>
      <GlobalStyle />
      <Helmet>
        <meta charSet="utf-8" />
        <html lang="nl" />
        <title>Arcady | Afstudeerportaal</title>
      </Helmet>
      <Header />
      <LandingPart />
      <Content />
      <AssignmentPart data={data} />
    </React.Fragment>
  </ThemeProvider>
)

export default IndexPage

export const query = graphql`
  query AllAssignments {
    allMarkdownRemark {
      edges {
        node {
          id
          frontmatter {
            title
            intro
            icon {
              childImageSharp {
                fixed(width: 64) {
                  ...GatsbyImageSharpFixed
                }
              }
            }
          }
          fields {
            slug
          }
        }
      }
    }
  }
`
