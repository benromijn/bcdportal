import React from 'react'
import GlobalStyle from '../components/styles/GlobalStyles'
import { graphql } from 'gatsby'
import { ThemeProvider } from 'styled-components'
import { theme } from '../components/styles/theme'
import BackgroundImage from 'gatsby-background-image'
import styled from 'styled-components'
import HomeLink from '../components/shared/HomeLink'
import HeaderLogo from '../components/shared/HeaderLogo'
import logo from '../images/bear_green.svg'
import Wrapper from '../components/shared/Wrapper'
import { Helmet } from 'react-helmet'

const StyledBackgroundImage = styled(BackgroundImage)`
  height: 100vh;
  h1 {
    color: white;
  }

  h2 {
    color: ${props => props.theme.colorPrimary};
  }
`

const SuccesPage = ({ data }) => {
  return (
    <ThemeProvider theme={theme}>
      <React.Fragment>
        <GlobalStyle />
        <Helmet>
          <meta charSet="utf-8" />
          <title>Arcady | You did it!</title>
        </Helmet>
        <StyledBackgroundImage fluid={data.bgImage.childImageSharp.fluid}>
          <HomeLink to="/">
            <HeaderLogo src={logo} />
          </HomeLink>
          <Wrapper flexposition="center">
            <h1>Gelukt.</h1>
            <h2>We hebben je gegevens! We gaan contact met je opnemen!</h2>
          </Wrapper>
        </StyledBackgroundImage>
      </React.Fragment>
    </ThemeProvider>
  )
}

export default SuccesPage

export const query = graphql`
  query {
    bgImage: file(relativePath: { eq: "arcady_bg.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1920) {
          ...GatsbyImageSharpFluid_tracedSVG
        }
      }
    }
  }
`
