---
title: 'Front-end Developer'
date: '2019-04-17'
intro: 'Een kennisplatform, gericht op delen en vermenigvuldigen van technische kennis. Uitdagend? Zeker!'
icon: './frontend.png'
heroPayoff: 'Bouw jij het Arcademy platform?'
---

Arcady is zich steeds verder aan het ontwikkelen als een kennisleider. En Arcadians houden ervan om kennis te delen, dat noemen wij de Arcademy. Daarom zoeken wij naar een platform gefocust op kennisdeling in de vorm van meerdere soorten content. In de vorm van tutorials, blogs, video’s en meer.

Op dit moment is er een simpele applicatie die wordt gebruikt tijdens het geven van trainingen: deelnemers kunnen hier online de stappen doorlopen om een gave applicatie te bouwen. Wij denken dat er potentie is voor meer. Jij ook?

Wij zoeken een afstudeerder die het tof lijkt om een nieuw platform op te zetten dat Arcady nog beter op de kaart zal zetten. Niet alleen ga je werken aan een gave architectuur, maar ga je ook nadenken over de business case en welke soorten content er kan worden gepubliceerd. Er is veel ruimte voor eigen inbreng voor features, en wij zorgen ervoor dat je de juiste ondersteuning krijgt.

You wanted to talk tech? .NET Core, CI/CD pipelines en Microsoft Azure. Er staat al een opzetje voor een frontend, gebouwd met Angular. Uiteraard mag je er je eigen stempel op zetten: kies de tools en technieken waar jij je in wil verdiepen.
