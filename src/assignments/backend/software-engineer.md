---
title: 'Software Engineer'
date: '2019-04-17'
intro: 'Onderzoek doen naar CMS-en, een proof of concept aanleveren en jouw app live zien gaan?'
icon: './backend.png'
heroPayoff: 'A perfect CMS does not exist... yet!'
---

Arcady heeft veel klanten, en wij bouwen vaak websites voor de gaafste bedrijven om ons heen. Echter hebben wij nog niet dé perfecte CMS kunnen vinden die gebruiksgemak combineert met een fijne SDK voor hardcore developers.

Er is op dit moment een mix van Umbraco, Contentful en andere CMS-systemen. Het liefste willen wij voor alle klanten dezelfde basis hebben, waar wij met onze Arcadians lekker aan door kunnen sleutelen.

Wij zoeken een afstudeerder die een onderzoek wil doen naar hét perfecte CMS, en hiermee ook een Proof-of-Concept wil implementeren voor Arcady en onze klanten. Je doet onderzoek naar allerlei factoren: hoe fijn is het voor de klant om er mee te werken? Is het flexibel? Kunnen we er meerdere websites op draaien? Kunnen we blijven innoveren met nieuwe technieken?

De stack is volledig afhankelijk van jouw onderzoek. Mocht je een Headless CMS willen gebruiken, dan kan je een gave frontend architectuur opzetten, bijvoorbeeld met React Gatsby of Angular. Toch een traditioneler CMS als Umbraco? Dan dagen wij je graag uit om met C# .NET een herbruikbaar project op te zetten dat we kunnen gebruiken voor tientallen klanten.
