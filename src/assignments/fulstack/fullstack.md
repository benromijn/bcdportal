---
title: 'Fullstacker' #used on the frontpage and as an title on the assignment page
date: '2019-04-17' #not used at the moment
intro: 'Kan jij meedenken en voortrekker worden in het ontwikkelen van een state of the art IoT platform?' # This is the intro text for the frontpage
icon: './fullstack.png' #Used on the frontpage
heroPayoff: 'Is IoT jouw "Thing"?'
---

Klanten van Arcady bevinden zich in een markt die continu aan het digitaliseren is. Hierbij is de zoektocht naar het in kaart brengen van data een veelgevraagd onderwerp. Tevens de combinatie kunnen maken met ‘domme’ hardware en ‘slimme’ software. Denk hierbij concreet aan het inzichtelijk maken van chloorwaarden voor de zwembaden van Landal Greenparks. Het in kaart brengen van de hoeveelheid smog er in de stad Zwolle hangt. Het meten van de vochtigheid van het cement op de bouwplaats om te zien of de fundering geheel uitgehad is.

Het doel van Arcady als technologiepartner is deze klanten te ondersteunen in het bouwen van slimme software, die mee schaalt met en door de vraag van de klant. Arcady levert de tools om sensordata real-time inzichtelijk te maken. Het is de combinatie met de klant die met zijn vak- en marktkennis de data op de juiste manier kan interpreteren middels onze tools.

Wij zoeken een afstudeerder die het leuk vindt om ruwe data om te zetten naar bruikbare data middels een dashboard met een sterk onderliggende architectuur. Jij onderzoekt wat er nodig is om dit te kunnen realiseren, en bouwt een Proof-of-Concept applicatie met de nieuwste technieken. Denk hierbij aan **.NET Core**, **Docker** en diverse building blocks van **Azure**. Ook kan je nadenken over de frontend van deze applicatie, met tools als **React, Vue** of **Angular**.
