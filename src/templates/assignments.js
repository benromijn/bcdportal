import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import AssignmentHero from '../components/AssignmentDetail/AssignmentHero'
import HeaderLogo from '../components/shared/HeaderLogo'
import logo from '../images/arcady_logo_white_64.png'
import HomeLink from '../components/shared/HomeLink'
import styled from 'styled-components'
import Container from '../components/shared/Container'
import AssignmentContent from '../components/AssignmentDetail/AssignmentContent'
import AssignmentImages from '../components/AssignmentDetail/AssignmentImages'
import AssignmentSignUp from '../components/AssignmentDetail/AssignmentSignUp'

const HeroText = styled.div`
  height: 100%;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  h1 {
    color: white;
  }

  h2 {
    padding-left: 50px;
    color: ${props => props.theme.colorPrimary};
  }
`

export default ({ data }) => {
  const post = data.markdownRemark
  return (
    <Layout helmet={post.frontmatter.title}>
      <AssignmentHero background={data.AssignmentImage}>
        <HomeLink to="/">
          <HeaderLogo src={logo} />
        </HomeLink>
        <Container>
          <HeroText>
            <h1>Hi, {post.frontmatter.title} to be.</h1>
            <h2>{post.frontmatter.heroPayoff}</h2>
          </HeroText>
        </Container>
      </AssignmentHero>
      <AssignmentContent data={post} />
      <AssignmentImages data={data.RelaxImages} />
      <AssignmentSignUp data={post} />
    </Layout>
  )
}

export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        heroPayoff
      }
    }
    AssignmentImage: file(relativePath: { eq: "lead-developer.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 1920) {
          ...GatsbyImageSharpFluid
        }
      }
    }
    RelaxImages: allFile(
      filter: { absolutePath: { regex: "/image-/" } }
      limit: 8
    ) {
      edges {
        node {
          childImageSharp {
            fluid(maxWidth: 500) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    }
  }
`
