// Check https://github.com/joellesenne/Blog-joellesenne/blob/721bccce911530628d1097a192a5b9828b0032ea/src/sendgrid/app.js for more info

// var data = JSON.stringify({
//   "personalizations": [
//     {
//       "to": [
//         {
//           "email": "b.romijn@arcady.nl",
//           //"email": "Hier de to mail via context?",
//           "name": "Ben Romijn"
//           //"name": "Hier de naam via context"
//         }
//       ],
//       "dynamic_template_data": {
//         "firstName": "via context", // Dus first en lastName splitsen?
//         "firstName": "Vladje",
//       }
//     }
//   ],
//   "from": {
//     "email": "hello@arcday.nl",
//     "name": "Arcady"
//   },
//   "reply_to": {
//     "email": "hello@arcday.nl",
//     "name": "Arcady"
//   },
//   "template_id": "d-65cd3984bdf44f4bafca10a7a3678ba6" // Deze zou al moeten kloppen
// });

// var xhr = new XMLHttpRequest();
// xhr.withCredentials = true;

// xhr.addEventListener("readystatechange", function () {
//   if (this.readyState === this.DONE) {
//     console.log(this.responseText);
//   }
// });

// xhr.open("POST", "https://api.sendgrid.com/v3/mail/send");
// xhr.setRequestHeader("authorization", "Bearer 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");
// Replace this with environment variables
// xhr.setRequestHeader("content-type", "application/json");

// xhr.send(data);

// Await response? Then callback?

exports.handler = function(event, context, callback) {
  const randomEmployees = [
    'Martijn de Ruiter',
    'Sytze Koster',
    'Ben Romijn',
    'Milan Bon',
    'Jasper Ikkink',
    'Wessel Loth',
    'Danny Hoeve',
    'Timo Goudzwaard',
  ]
  const randomNumber = Math.floor(Math.random() * 8)
  callback(null, {
    statusCode: 200,
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
    body: JSON.stringify({ employee: randomEmployees[randomNumber] }),
  })
}
