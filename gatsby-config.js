module.exports = {
  siteMetadata: {
    title: `Arcady Afstudeer Portaal`,
    description: `Finish your education with a blast`,
    author: `Arcady.nl by Ben Romijn`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 590,
            },
          },
          'gatsby-remark-copy-linked-files',
        ],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `assignments`,
        path: `${__dirname}/src/assignments`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Afstuderen bij Arcady`,
        short_name: `Afstuderen`,
        start_url: `/`,
        background_color: `#FFF`,
        theme_color: `#00a667`,
        display: `standalone`,
        icon: `src/images/bear_green.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.app/offline
    'gatsby-plugin-offline',
  ],
}
